trait Equal {
  def is_equal(x: Any): Boolean
  def is_not_equal(x: Any): Boolean = !is_equal(x)
}

class Point(xc: Int, yc: Int) extends Equal {
  var x: Int = xc
  var y: Int = yc

  def is_equal(obj: Any) = obj.isInstanceOf[Point] && obj.asInstanceOf[Point]
    .x == y
}


object Demo {
  def main(args: Array[String]): Unit = {
    val p1 = new Point(2, 3)
    val p2 = new Point(2, 2)
    val p3 = new Point(2, 5)

    println(p1.is_not_equal(p2))
    println(p2.is_not_equal(p3))
    println(p1.is_not_equal(p3))
  }
}
