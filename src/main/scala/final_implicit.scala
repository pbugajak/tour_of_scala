object Opa extends App {
  final implicit class AnyOps(private val self: Any) extends AnyVal {
    def styled(style:String): String = style + self + Console.RESET

    def red: String = styled(Console.RED)
    def green: String = styled(Console.GREEN)
    def blue: String = styled(Console.BLUE)
  }

  println(List(1, 2, 3).red)
  println(List(1, 2, 3).green)
  println(List(1, 2, 3).blue)
  println(List(1, 2, 3).styled(Console.MAGENTA))
  println(List(1, 2, 3).styled("Not type safe space"))
  println("-" * 40)


  println("-" * 40)
}



