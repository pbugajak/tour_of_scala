trait Iterable[A] {
  def foldLeft[B](z: B)(op: (B, A) => B): B
}

case class WeeklyWeatherForecast(temperatures: Seq[Double]) {

  private def convertCtoF(temp: Double) = temp * 1.8 + 32

  def forecastInFahrenheit: Seq[Double] = temperatures.map(convertCtoF) // <-- passing the method convertCtoF
}

object higher_order_function {
  def main (args: Array[String]): Unit = {

    val weekly_weather_temperature = Seq(14.00, 51.41, 51.21, -41.31, 12.00, 14.21)
    val weeklyWeatherForecast = WeeklyWeatherForecast(weekly_weather_temperature)
    println(weeklyWeatherForecast.forecastInFahrenheit)

//    NESTED METHODS

  def factorial(x: Int): Int = {
    def fact(x: Int, accumulator: Int): Int = {
      if (x <= 1) accumulator
      else fact(x - 1, x * accumulator)
      }
    fact(x, 1)
    }

  println(factorial(5))

  val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
//  val res = numbers.foldLeft(0)((m, n) => m + n)

//  println(res)
//  val res_one = numbers.foldLeft(0)((_, _) => _ + _)
  def foldLeft1[A, B](as: List[A], b0: B, op: (B, A) => B) = ???
  def res_way = foldLeft1[Int, Int](numbers, 0, _ + _)
  println(res_way)

  }
}
