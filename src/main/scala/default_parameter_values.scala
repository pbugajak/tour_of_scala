import scala.collection.mutable.ArrayBuffer


//  CLASS COMPOSITION WITH MIXINS
//  Mixins are traits which are used to compose a class
abstract class AbsIterator {
  type T
  def has_next: Boolean
  def next(): T
}

class StringIterator(s: String) extends AbsIterator {
  type T = Char
  private var i = 0
  def has_next = i < s.length
  def next() = {
    val ch = s charAt i
    i += 1
    ch
  }
}

trait RichIterator extends AbsIterator {
  def foreach(f: T => Unit): Unit = while(has_next) f(next())
}

class RichStringIter extends StringIterator("Scala") with RichIterator

abstract class A {
  val message: String
}

class B extends A {
  val message = "This is instance of class B"
}

trait C extends A {
  def loud_message = message.toUpperCase()
}

class D extends B with C


//    TRAITS

trait Iterator[A] {
  def has_next: Boolean
  def next(): A
}

class IntIterator(to: Int) extends Iterator[Int] {
  private var current = 0

  override def has_next: Boolean = current < to

  override def next(): Int = {
    if (has_next) {
      val t = current
      current += 1
      t
    } else 0
  }
}

//    SUBTYPING

trait Pet {
  val name: String
}

class Cat(val name: String) extends Pet
class Dog(val name: String) extends Pet


object default_parameter_values {
  def main(args: Array[String]): Unit = {
    def log(message: String, level: String = "INFO") = println(s"$level: $message")
    log("System starting")
    log("We should aveare someone", "WARNING")

//    NAMED_ARGUMENTS
    def print_name(first: String, last: String = "Kowalski"): Unit = {
      println(first + " " + last)
    }
    print_name("Michal", "no Michal")

//    print_name(last = "Nowak", "A moze jeszcze cos")
//    this won't compile cause we will give argument
//    which is considered as one next to our last argument]

  val iterator = new IntIterator(10)
  iterator.next()               // It will return 0
  println(iterator.next())     // It will print 1
  iterator.next()               // It will return 2

  println("*" * 40)

  val dog = new Dog("Harry")
  val cat = new Cat("Sally")

  val animals = ArrayBuffer.empty[Pet]
  animals.append(dog)
  animals.append(cat)
  animals.foreach(pet => println(pet.name))

//    TUPLES
  val ingredient = ("Sugar", 25)
  println(ingredient._2)

  val (name, quanity) = ingredient
  println(name)

  val planets =
    List(("Mercury", 57.9), ("Venus", 108.2), ("Earth", 149.6),
         ("Mars", 227.9), ("Jupiter", 778.3))
  planets.foreach {
    case (planet, distance) =>
      println(s"Our $planet is $distance milion kilometers from the sun")
    case _ =>

//    From the other hand we might define above example as:
//    case class Planet(name: String, distance: Double)
//    rather than using tuple

//    This will print only one line with case where first argument is equal to string == Earth
//    case ("Earth", distance) =>
//    println(s"Our $planet is $distance milion kilometers from the sun")
//    case _ =>
    }

  val pairs_of_numbers = List((2, 5), (2, 7), (5, 8), (-41, 32))
  for ((a, b) <- pairs_of_numbers) {
    if (a > b)
      println("a: " + a)
    else
      println("b: " + b)
    }

  val b = new B
  println(b.message)

  val d = new D
  println(d.message)
  println(d.loud_message)

  val richStringIter = new RichStringIter
  richStringIter.foreach(println)
  }
}
