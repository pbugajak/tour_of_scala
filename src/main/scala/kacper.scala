class kacper {
  def some_string_operations(nickname: String = "Example") : String = {
    println("Tester".equals(nickname)) // is string equal
    val nickname_array = nickname.toArray
    for (v <- nickname_array) {
        println(v)
    }
    ""
  }
}
