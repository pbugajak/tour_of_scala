import scala.util.Random
import scala.math._
import scala.util.matching.Regex


//abstract class Animal {
//  def name: String
//}
//
//abstract class Pet extends Animal {}
//
//class Cat extends Pet {
//  override def name: String = "Cat"
//}
//
//class Dog extends Pet {
//  override def name: String = "Dog"
//}
//
//class Lion extends Pet {
//  override def name: String = "Lion"
//}
//
//class PetContainer[P <: Pet](p: P) {
//  def pet: P = p
//}


class Fruit
class Apple extends Fruit
class Banana extends Fruit


class Stack[A] {
  private var elements: List[A] = Nil
  def push (x: A): Unit =
    elements = x :: elements
  def peek: A = elements.head
  def pop(): A = {
    val current_top = peek
    elements = elements.tail
    current_top
  }
}


case class UserForComprehensions(name: String, age: Int)


class SenderEmail(val user_name: String, val domain_name: String)

object SenderEmail {
  def from_string(email_string: String): Option[SenderEmail] = {
    email_string.split('@') match {
      case Array(a, b) => Some(new SenderEmail(a, b))
      case _           => None
    }
  }
}


case class Circle(radius: Double) {
  import Circle._
  def area: Double = calculate_area(radius)
}

object Circle {
  private def calculate_area(radius: Double): Double = Pi * pow(radius, 2.0)
}


sealed abstract class Furniture
case class Couch() extends Furniture
case class Chair() extends Furniture


abstract class Device
case class Phone(model: String) extends Device {
  def screen_off = "Turning screen off"
}


case class Computer(model: String) extends Device {
  def screen_saver_on = "Turning on screen saver"
}


abstract class Notification
case class Email(sender: String, title: String, body: String) extends Notification
case class SMS(caller: String, message: String) extends Notification
case class VoiceRecording(contact_name: String, link: String) extends Notification


class pattern_matching {
}


object pattern_matching {
  def main(args: Array[String]) : Unit = {
    println("*" * 40)
//    val evenOrOdd = (i: Int) match {
//      case 1 | 3 | 5 | 7 | 9 => println("odd")
//      case 2 | 4 | 6 | 8 | 10 => println("even")
//      case _ => println("some other number")
//    }
//    def is_even_or_odd(i: Int): Unit = i match {
//      case i if i % 2 != 0 => println("odd")
//      case i if i % 2 == 0 => println("even")
//      case i if i % 2 != 0 => println("whatever")
//}
//
//    is_even_or_odd(4)
//    is_even_or_odd(27)
//    println("*" * 40)
//
//    val x: Int = Random.nextInt(6)
//    def match_test(x: Int):String =
//      x match {
//        case 0 => "zero"
//        case 1 => "one"
//        case 2 => "two"
//        case _ => "anything"
//      }
//
//    println(match_test(5))
//    println(match_test(x))
//    println("*" * 40)
//
//    def show_notification(notification: Notification): String = {
//      notification match {
//        case Email(sender, title, _) =>
//          s"You got an email from $sender with title: $title"
//        case SMS(number, message) =>
//          s"You got a SMS from $number, Messeage: $message"
//        case VoiceRecording(name, link) =>
//          s"You recived a voice recording from $name, check the link for it $link"
//      }
//    }
//
//    def show_important_notification(notification: Notification, important_people: Seq[String]): String = {
//      notification match {
//        case Email(sender, _, _) if important_people.contains(sender) =>
//          "You got an email from special someone!"
//        case SMS(number, _) if important_people.contains(number) =>
//          "You got a SMSMS from special someone!"
//        case other =>
//          show_notification(other)
//      }
//    }
//
//    val some_sms = SMS("41412", "hello, it")
//    val some_email = Email("pysiaczek", "co na kolecje?", ":*")
//    println(show_notification(some_sms))
//    println(show_notification(some_email))
//    println("*" * 40)
//
//    val important_people = Seq("8862008", "123423", "41241")
//    val some_sms_important_not = SMS("41412", "hello, it")
//    val some_email_important = Email("8862008", "gramy", ":*")
//    val some_voice_recording = VoiceRecording("tutut", "tutut")
//    println(show_important_notification(some_sms_important_not, important_people))
//    println(show_important_notification(some_email_important, important_people))
//    println(show_important_notification(some_voice_recording, important_people))
//
//    println("*" * 40)
//
//    def go_idle(device: Device) = device match {
//      case p: Phone => p.screen_off
//      case c: Computer => c.screen_saver_on
//    }
//    val some_computer = Computer("THX-124")
//    println(Computer("TGS-131").screen_saver_on)
//    println(go_idle(Phone("SDT-765")))
//
//    println("*" * 40)
//
//    def find_place_to_sit(piece: Furniture): String = piece match {
//      case a: Couch => "Lie on the couch"
//      case b: Chair => "Sit on the chair"
//    }
//    println(find_place_to_sit(Chair()))
//    println(find_place_to_sit(Couch()))
//
//    println("*" * 40)

//    val circle1 = Circle(4.0)
//    println(circle1.area)
//    println(Circle(3.2).area)
//
//    println("*" * 40)
//
//    val scala_center_email = SenderEmail.from_string("scala.center@onet.eueueu")
//    scala_center_email match {
//      case Some(email) => println(
//        s"""Registered an email
//           |Username: ${email.user_name}
//           |Domain: ${email.domain_name}
//           |""".stripMargin)
//      case None => println("Error: could not parse email")
//    }
//
//    println("*" * 40)
//
//    val number_pattern: Regex = "[0-9]".r
//
//    number_pattern.findFirstMatchIn("fsadgfasdfsd") match {
//      case Some(_) => println("Password OK")
//      case None    => println("Password must contains a number")
//    }

//    println("*" * 40)
//
//    val key_value_pattern: Regex = "([0-9a-zA-Z- ]+): ([0-9a-zA-Z-#()/. ]+)".r
//    val input_string: String =
//      """background-color: #A03300;
//        |background-image: url(img/header100.png);
//        |background-position: top center;
//        |background-repeat: repeat-x;
//        |background-size: 2160px 108px;
//        |margin: 0;
//        |height: 108px;
//        |width: 100%;""".stripMargin
//
//    for (pattern_match <- key_value_pattern.findAllMatchIn(input_string)) {
////      println(
////        s"""
////           |
////           |
////           |""".stripMargin)
//      println(s"key: ${pattern_match.group(1)}, value: ${pattern_match.group(2)}")
//    }
//
//    println("*" * 40)
//
//    object CustomerID {
//
//      def apply(name: String) = s"$name---${Random.nextLong}"
//
//      def unapply(customerID: String): Option[String] = {
//        val stringArray: Array[String] = customerID.split("---")
//        if (stringArray.tail.nonEmpty)
//          Some(stringArray.head)
//        else
//          None
//      }
//    }
//
//    val customer_1ID = CustomerID("Makalele")
//    customer_1ID match {
//      case CustomerID(name) => println(name)
//      case _                => println("Couldn't extract a customer ID")
//    }
//    println("*" * 40)
//
//    val customer2ID = CustomerID("Nico")
//    val CustomerID(name) = customer2ID
//    println(name)
//    println("*" * 40)
//
//    val real_name = CustomerID.unapply(customer2ID).get
//    println(real_name)
//    println(CustomerID.unapply(customer2ID).get)
//    println("*" * 40)
//
//    val user_base = List(
//      UserForComprehensions("Travis", 28),
//      UserForComprehensions("Kelly", 33),
//      UserForComprehensions("Jennifer", 44),
//      UserForComprehensions("Dennis", 23)
//    )
//
//    val ninenties_are_the_best =
//      for (user <- user_base if user.age >= 21 && user.age < 31)
//        yield user.name
//
//    ninenties_are_the_best.foreach(println)
//    println("*" * 40)
//
//    def foo(n: Int, v: Int) =
//      for (i <- 0 until n;
//           j <- 0 until n if i + j == v)
//        yield (i, j)
//
//    foo(10, 10) foreach {
//      case (i, j) => println(s"($i, $j)")
//    }
//    println("*" * 40)

//    def list_of_duplicates[A](x: A, length: Int): List[A] = {
//      if (length < 1)
//        Nil
//      else
//        x :: list_of_duplicates(x, length -  1)
//    }
//
//    println(list_of_duplicates[Double](4.0, 2))
//    println("*" * 40)
//
//    val days = List("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
//
//    // Pattern match
//    days match {
//      case firstDay :: otherDays =>
//        println("The first day of the week is: " + firstDay)
//      case Nil =>
//        println("There don't seem to be any week days.")
//    }

//    println("*" * 40)

//    val stack = new Stack[Int]
//    stack.push(1)
//    println(stack.pop)
//    stack.push(3)
//    println(stack.pop)
//    println("*" * 40)
//
//    val stack_of_fruit = new Stack[Fruit]
//    val apple = new Apple
//    val banana = new Banana
//
//    stack_of_fruit.push(apple)
//    stack_of_fruit.push(banana)
//    println(stack_of_fruit.pop)
//    println("*" * 40)

//    val dog_container = new PetContainer[Dog](new Dog)
//    val cat_container = new PetContainer[Cat](new Cat)
//    println(cat_container)
    println("*" * 40)


  }
}
