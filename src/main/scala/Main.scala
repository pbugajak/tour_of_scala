import java.util
import scala.collection.mutable
//import scala.jdk.CollectionConverters._       // 2.13


object Main {
  def main(args: Array[String]): Unit = {
    println("Hello, Scala developer!")

    println("-" * 40)

//    final impicit class AnyOps(private val self: Any) extends AnyVal {
//      def style(style:String): String = style + self + Console.RESET
//
//      def red: String = styled(Console.RED)
//      def green: String = styled(Console.GREEN)
//      def blue: String = styled(Console.BLUE)
//    }

    println("-" * 40)

//    val c: Char = 65
//    println(c)
//
//    val s: Seq[Char] = "hello world"
//    println(s)
//
//    val b: Byte = 25
//    val i: Int = b
//    println(i)
//
//    val big: BigInt = 37
//    println(big + 1)
//
//    println("-" * 40)
//
//    final case class Apple(weight_in_grams: Int, color: String)
//    final case class Orange(weight_in_grams: Int)
//
//    def function(orange: Orange): Unit =
//      println(orange)
//
//    implicit def AppleWrapper(apple: Apple): AppleWrapper =
//      new AppleWrapper(apple)
//
//    final class AppleWrapper(apple: Apple) {
//      def toOrange: Orange =
//        Orange(apple.weight_in_grams)
//    }
//
//
//    implicit def apple_can_be_used_as_orange(apple: Apple): Orange =
//      Orange(apple.weight_in_grams)
//
//    function(
////      apple_can_be_used_as_orange(
//      AppleWrapper(
//        Apple(
//          weight_in_grams = 500,
//          color = "red"
//        )
//      ).toOrange
//    //      )
//    )
//    println("-" * 40)




////    FAMILIES OF DATA STRUCTURE
//    sealed abstract class Entity
//    object Entity {
//      final case class Person(name: String, age: Int) extends Entity
//      final case class Employee(id: Int, person: Person) extends Entity
//      case object Unidentified extends Entity
//    }
//
//    val p: Entity = Entity.Person(name = "Alice", age = 31)
//    val e: Entity = Entity.Employee(id = 1244, person = new Entity.Person(name = "Bob", age = 41))
//
//    println(p)
//    println(e)
//    println("-" * 40)
//
//    def should_pay_salary(e: Entity): Boolean = e match {
//      case _: Entity.Employee => true
//      case _                  => false
//    }
//
//    println(should_pay_salary(p))
//    println(should_pay_salary(e))
//
//    println("-" * 40)
//
//    def add_one_and_turn_into_string(option: Option[Int]): Option[String] = {
//      option match {
//        case Some(n) => Some((n + 1).toString)
//        case None    => None
//      }
////      option.map(_ + 1)
////      option.map(n => (n + 1).toString)
////      option.map(_ + 1).map(_.toString)   // !!!
//    }
//
//    val option1: Option[Int] = Some(1337)
//    println(option1)
//    println(add_one_and_turn_into_string(option1))
//
//    val option2: Option[Int] = None
//    println(option2)
//    println(add_one_and_turn_into_string(option2))
//
//    println("-" * 40)
//
//    val people: List[Entity.Person] =
//      List(
//        Entity.Person("Alice", 25),
//        Entity.Person("Bob", 26)
//      )
//
//    println(people.find(_.name.toLowerCase.startsWith("a")))
//    println(people.find(_.name.toLowerCase.startsWith("g")))
//    println(people.find(_.name.toLowerCase.startsWith("a")).get)
//    println(people.find(_.age % 2 != 0))
//    println("-" * 40)
//
//    def age(e: Entity): Option[Int] = e match {
//      case Entity.Employee(_, Entity.Person(_, result)) => Some(result)
//      case Entity.Person(_, result)                     => Some(result)
//      case Entity.Unidentified                          => None
//    }
//
//    println(age(e))
//    println(age(p))
//    println(age(Entity.Unidentified))
//    println("-" * 40)
//
//    age(e)
//      .map(_ + 1)
//      .map(_.toString)
//      .foreach(println)
//    println("-" * 40)
//
//    val sum_of_all_ages: Option[Int] =
//      for {
//        ageE <- age(e)
//        ageP <- age(p)
//      } yield ageE + ageP
//
//    val average_age: Option[Double] =
//      sum_of_all_ages.map(_.toDouble).map(_ / 2)
//
//    sum_of_all_ages.foreach(println)
//    average_age.foreach(println)
//
//    println("-" * 40)

//    final case class Person(name: String, var age: Int)     // data structured class, so like python p1.age
//
//    val p1 = new Person("Bob", 12)
//    val p2 = new Person("Bob", 12)
//
//    println(p1 == p2)
//    println(p1)
//    println("-" * 40)
//
//    println(p1.age, p1.name)
//    println("-" * 40)
//
//    val Person(n, a) = p1
//    println(n)
//    println(a)
//    println("-" * 40)
//
//    println(Set(p1, p1, p1))
//    println("-" * 40)
//
//
//    val p3 =
//      p1.copy(age = 33, name = "Vlad")
//
//    println(p1)
//    println(p3)
//    println("-" * 40)
//
//    p3.age = 45
//    println(p3.age)
//
//    println("-" * 40)
//
//    val p4 = Person("Alice", 25)
//    val p5 = Person("Alice", 26)
//    val p6 = Person("Alice", 27)
//
//    val set = Set(p4, p5, p6)
//    set.foreach(println)
//    println("-" * 40)
//
//    p6.age = 25
//    set.foreach(println)
//
//    println("-" * 40)

//    object Calculator {
//      def substract(a: Int, b: Int): Int =
//        a - b
//    }
//
//    def substract(a: Int, b: Int, calculator: Calculator.type): Int = {
//      Calculator.substract(12, 4)
////      println(calculator)
//    }
//    println("-" * 40)


//    object Calculator extends Add with Substract
//
//    trait Add {
//      def add(a: Int, b: Int): Int = a + b
//    }
//
//    trait Substract {
//      def substract(a: Int, b: Int): Int = a - b
//    }
//
//    println(Calculator.add(1, 5))
//    println("-" * 40)
//
////    class Calculator(a: Int) extends Add with SubstractWithAdd {
//    class Calculator(a: Int) extends SubstractWithAdd {
//      def add(b: Int): Int = a + b
//      override def substract(b: Int): Int = a - b
//    }
//
//    trait Add {
//      def add(b: Int): Int
//    }
//
//    trait SubstractWithAdd extends Add {
//      def substract(b: Int): Int = add(-b)
//    }
//
//    println("-" * 40)


////    Can be understood as static method
//    object Calculator {
//      def calculators_hidden: Int = _calculator_hidden_value
//      private var _calculator_hidden_value: Int = 0
//      def calculators_hidden_=(newValue: Int): Unit =
//        _calculator_hidden_value = newValue
//
//      var calculators_created: Int = _  // default value for _ is 0
//    }
//
////    CONSTRUCTOR
//    class Calculator(a: Int) {
////      println("Is exucitive during construction")
//      Calculator.calculators_created += 1
//      Calculator.calculators_hidden_=(Calculator.calculators_hidden + 1)
//
//      def add(b: Int): Int = a + b
//      def substract(b: Int): Int = a -b
//    }
//
//    val c1 = new Calculator(a = 1)
//    val c2 = new Calculator(a = 1)
//    val c3 = new Calculator(a = 1)
//    val c4 = new Calculator(a = 1)
//
//    val result = c1.add(b = 2)
//    println(result)
//    println(c1.substract(2))
//    println("-" * 40)
//
//    println(Calculator.calculators_created)
//    println("-" * 40)
//
//    println(Calculator.calculators_hidden)
//    println("-" * 40)

//    println(List(1, 2, 3, 4).foldLeft(0) { (acc, current) =>
//      acc + current
//      }
//    )
//
////    println(List(1, 2, 3, 4).sum)
//
//    println(List(1, 2, 3, 4).foldLeft(0)(_ * _))
//    println("-" * 40)
//
//    def plus(a: Int, b: Int): Int = a + b
//
//    val result_of_plus=plus(a = 1, b = 2)
//
//    println(result_of_plus)
//    println("-" * 40)
//
//    def plus_curried_function(a: Int)(b: Int): Int = a + b    //function currying - functions that carries lists of arguments
//
//    println("-" * 40)
//
////    : Int => Int means that this function return function which take Int and returns Integer, so it means that this is higher-order function
//    def plus_better_one(a: Int): Int => Int = b => a + b
//
//    val second_function = plus_better_one(1)    // a = 1
//    val result_of_plus_better_one = second_function(2)    // b = 2
//    println(result_of_plus_better_one)
//
//    val result_better_way = plus_better_one(1)(3)
//    println(result_better_way)
//
//    println("-" * 40)

    //    object MyCollection {
//      def apply(ints: Int*): Seq[Int] =
//        ints
//    }
//
//    val c1 = MyCollection()
//    println(c1)
//
//    val c2 = MyCollection(1, 2, 3, 4)
//    println(c2)
//
//    println("-" * 40)
//
//    // Object bellow disallow us to create an empty list
//    object MyCollectionUpgrade {
//      def apply(first: Int, rest: Int*): Seq[Int] =
//        first +: rest
//    }
//
//    val c3 = MyCollectionUpgrade(1)
//    println(c3)
//
//    println("-" * 40)
//
//    val hashSet = {
//      val result = new util.HashSet[Int]
//
//      result.add(1)
//      result.add(2)
//      result.add(3)
//      result.add(4)
//      result.add(3)
//      result.add(2)
//      result.add(1)
//
//      result
//    }
//
//    println(hashSet)
//
//    println("-" * 40)
//
//    val scala_set = hashSet.asScala
//    println(scala_set)
//
//    println("-" * 40)
//
//    val java_set = scala_set.asJava
//    println(java_set)
//
//    println("-" * 40)

//    println(List(1, 2, 3).to(Vector))
//    println(List(1, 2, 3, 4, 6, 8, 645, 64564, 645, 645, 132).to(Set))
//
//    println("-" * 40)
//
//    val map = Map(1 -> "I", 2 -> "II", 3 -> "III")
//    println(map + (4 -> "IV"))
//    println(map + (3 -> "Been there done that"))
//    println(map ++ Map(3 -> "Been there", 4 -> "Done that"))
//
//    println("-" * 40)
//
//    val set = Set(1, 2, 3)
//    println(set)
////    println(0 +: set :+ 4)  // This won't work, there is no  order, you can only add staff, there is also a no duplicates
//    println(set + 0)
//    println(set ++ Set(4, 5, 6) ++ set)
//
//    println("-" * 40)
//
//    val vector = Vector(1, 2, 3)    // vector is an index collection behind it is actually tree of arrays
//    println(vector)
//    println(0 +: vector :+ 4)
//    println(vector ++ Vector(4, 5, 6))
//
//    println("-" * 40)
//    println("-" * 40)
//
//    val list = List(1, 2, 3)
//    print(list)
//    println(0 :: list)  // list.::(0) if it ends with : it means that 0 is assigned to list, right associative
//    println(0 +: list)  // list.+:(0)
//    println(0 +: list :+ 4)
//    println(list ::: List(4, 5, 6)) //concatitave lists
//    println(list.++(List(4, 5, 6)))   // to add list of arguments to the list
//    println(1 :: 2 :: 3 :: List.empty)
////    println(1 :: 2 :: 3 :: Nil)   // Ssame as aboth one
//
//    println("-" * 40)
//

//
//
//    val chees_board =
//      for {
//        c <- 'a' to 'h'
//        if c <= 'b'
//        n <- 0 to 8
//      } yield c -> n
//
////    chees_board.foreach(println)
//
//    val chees_board2 =
//      ('a' to 'h').flatMap { c =>
//        (0 to 8).filter(_  % 2 == 0).map { n =>
//          c -> n
//        }
//      }
//
////    chees_board2.foreach(println)
//
//    def fibonacci(n: Int): Int =
//      if (n <= 1) 1 else fibonacci(n - 1) * n
//
//    1 to 9 map fibonacci foreach println
//
//    println("-" * 40)
//
//    def fibonacci_tail(n: Int): Int = {
//      @scala.annotation.tailrec
//      def loop(x: Int, acc: Int): Int =
//        if ( x <= 1 )
//          acc
//        else
//          loop(
//            x = x - 1,
//            acc = acc * x
//          )
//
//      loop(x = n, acc = 1)
//    }
//
//    1 to 9 map fibonacci_tail foreach println
//
//    println("-" * 40)

//    val even_more_interesting_too =
//      ('a' to 'h').map { c =>
//        (0 to 8).map { n =>
//          c -> n
//        }
//      }.flatten
//      Without .flatten it prints vectors for each a from 0 to 8

//    even_more_interesting_too.foreach(println)

//    val even_more_interesting_too2 =
//      ('a' to 'h').flatMap { c =>
//        (0 to 8).map { n =>
//          c -> n
//        }
//      }
//
//    even_more_interesting_too2.foreach(println)
//
//    println("-" * 40)
//
//    val even_more_interesting =
//      (for (c <- 'a' to 'h')
//        yield for (n <- 0 to 8)
//        yield c -> n).flatten
//
//    even_more_interesting.foreach(println)
//
//    println("-" * 40)
//
////    FUNCTIONS
//    (z: Int) => z + 1
//    val add_one = (x: Int) => x + 1
////      if we type println(add_one(1)) we will get 2,
////      understanding of logic "(x: Int) => x + 1":
////      x which is integer is like x plus 1
//
//    println("-" * 40)

//      METHODS
//
//    def simple_adding(x: Int, y: Int): Int = x + y
//    println(simple_adding(2, 5))
//
//    def get_square_of_input(x: Double): String = {
//      val square = x * x
//      square.toString
//    }
////  Something like we required x which is integer, and y, also integer.
////  The output will be integer as well,
////  This integer is equal to adding x and y
//
//    println("-" * 40)
//
////      CLASSES
//    class Greeter(prefix: String, sufix: String) {
//      def greet(name: String): Unit =
//        println(prefix + name + sufix)
//}
////      Creating instance of class
//    val welcoming = new Greeter("Welcome ", " My friend")
//    welcoming.greet("Andrzej")
//
//    println("-" * 40)

//      CASE CLASSES
//      Instances are immutable, useful for pattern matching
//
//    case class Point(x: Int, y: Int)
//
//    val first_point = Point(1, 2)
//    val second_point = Point(2, 2)
//    val third_point = Point(1, 2)
//
//    if (first_point == second_point) {
//      println(first_point + " and " + second_point + " are the same.")
//    } else {
//      println(first_point + " and " + second_point + " are not the same. Tfu !")
//    }
//
//    if (first_point == third_point) {
//      println(first_point + " and " + third_point + " are the same.")
//    } else {
//      println(first_point + " and " + third_point + " are not the same.")
//    }
//
//    println("-" * 40)
//
////    OBJECTS
////    Single instance of their own definitions
////    Could be understand as singletons of their own classes
//
//    object factory_of_id {
//      private var counter = 0
//
//      def create(): Int = {
//        counter += 1
//        counter
//      }
//    }
//
//    val new_id = factory_of_id.create()
//    val newer_id = factory_of_id.create()
//    println(newer_id)
//
//    println("-" * 40)
//
////      TRAITS -> Demo.scala
////      abstract data types which contains certain fields and methods.
////      In Scala inheritance, a class can only extend one other class
////      while actually class can extend multiple traits
//
//    trait Greeter_trait {
//      def greet(name: String): Unit =
//        println("Hello", name, ":)")     // This will produce list !!
//    }
//
////    Here it is required to know two important method of Scala, which are used in the following example.
////
////    obj.isInstanceOf [Point] To check Type of obj and Point are same are not.
////    obj.asInstanceOf [Point] means exact casting by taking the object obj type and returns the same obj as Point type.
//
//
//    class DefaultGreeter extends Greeter_trait
//
//    class CustomizableGreeter(prefix: String, postfix: String) extends Greeter_trait {
//      override def greet(name: String): Unit = {
//        println(prefix + name + postfix)
//      }
//    }
//
//    val greeter_trait = new DefaultGreeter()
//    greeter_trait.greet("Mariusz !")
//
//    val another_greeter_trait = new CustomizableGreeter("Aloha ", " From other side")
//    another_greeter_trait.greet("Andrzeju")
//
//    println("-" * 40)
//
//
//
//    println("-" * 40)
//
//
//    val x_x: Int = 1 + 1
//
//    var y: Int = 2 + 1
//    y = 4
//    println(y)
//
//
//
//    println("-" * 40)
////    RANGE
//    Range(start = 0, end = 10).foreach(println)
//    println("-" * 40)
//
//    Range(start = 0, end = 10, step = 2).foreach(println)
//    println("-" * 40)
//
//    Range(start = 0, end = 10, step = 2).foreach(println)
//    println("-" * 40)
//
//    Range.inclusive(start = 0, end = 9).foreach(println)
//    println("-" * 40)
//
//
//    0.until(10).by(2).foreach(println)
//    println("-" * 20 + "pretty same" + "-" * 20)
//
//    0 until 10 by 2 foreach println
//    println("-" * 40)
//
//    val interesting_gg =
//      for (i <- 0 to 9)
//        yield println(i)
//    println("-" * 40)
//
//
//    val (even, odd) = List(1, 2, 3, 4, 5, 6).partition(_ % 2 ==0)
//    println(even)
//    println(odd)
//
//    println("-" * 40)
//
//    Map(1 -> "I", 2 -> "II", 3 -> "III")
//      .map {
//        case(key, value) => key -> value.toLowerCase
//      }
//      .foreach(println)
//
//    println("-" * 40)
//
////    println(Map((1, "I"), (2, "II"), (3, "III")))
//    Map(1 -> "I", 2 -> "II", 3 -> "III")
//      .map { tuple2 =>
//        val key = tuple2._1
//        val value = tuple2._2
//
//        key -> value.toLowerCase
//      }
//      .foreach(println)
//
//    println("-" * 40)
//    //   combinattion of map and flatten == flatMap
//    //    println(matrix.flatten.foreach(println))
//    val matrix_for_flatMap = {
//      List(
//        List(1, 2, 3),
//        List(4, 5, 6),
//        List(7, 8, 9)
//      )
//    }
//    matrix_for_flatMap.map(_.reverse).flatten.foreach(println)
//    println("-" * 40)
//
//    matrix_for_flatMap.flatMap(_.reverse).foreach(println)
//
//    println("-" * 40)
//
//    val matrix =
//      List(
//        List(1, 2, 3),
//        List(4, 5, 6),
//        List(7, 8, 9)
//      )
//
//    matrix.foreach(println)
//    println("-" * 40)
//
//
//    matrix.flatten.foreach(println)
////    everything is an expression so: combinattion of map and flatMap is flatten
////    println(matrix.flatten.foreach(println))
//    println("-" * 40)
//
////    flatMap
//    List(1, 2, 3)
//      .flatMap { n =>
//        if (n % 2 == 0)
//          List()
//        else
//          List(n)
//      }
//      .foreach(println)
//
//    println("-" * 40)
//
//    List(1, 2, 3).map(_ + 1).foreach(println)
//    println("-" * 40)
//
//    List(1, 2, 3).filter(_ % 2 != 0).foreach(println)
//    println("-" * 40)
//
////      METHOD CHAIN NOTATION
//    List(1, 2, 3)
//      .map(_ + 1)
//      .filter(_ % 2 == 0)
//      .foreach(println)
//
//    println("-" * 40)
//    println(Seq(1, 2, 3))
//    println(List(1, 2, 3)(1))
//    println(IndexedSeq(1, 2, 3)(1))
//    println(IndexedSeq(1, 2, 3))
//    println(Vector(1, 2, 3))
//    println(Set(1, 2, 3, 4, 4))
//    println(Set(1, 2, 3, 4, 4, 4, 5))
//    println(scala.collection.immutable.HashSet(1, 2, 3, 4, 4, 4, 5))
//
//    println("-" * 40)
//
//    val bla = mutable.ArraySeq(1, 2, 3)
//    println(bla)
//
//    object my_stuff {
//      val inside = 1337
//    }
//
//    import my_stuff.inside
//    println(inside)
//
//    println("-" * 40)
//
////    COLLECTIONS
//    List(1, 2, 3)
//
//    val fruits = Array("apple", "orange", "banana")
//    println(fruits(2))
//    println(fruits.apply(2))
//
//    fruits(2) = "melon"
//    println(fruits(2))
//
//    println("-" * 40)
//
//    fruits.foreach(println)
//    println("-" * 40)
//
//    fruits.mapInPlace(_.reverse).foreach(println)
//    println("-" * 40)
//
//    fruits.foreach(println)
//    println("-" * 40)
//
//
////    EXCEPTIONS
//    val result_of_divining_by_zero: Int =
//      try 1 / 0
//      catch {
//        case e: ArithmeticException => 0
////          println(Console.RED + e.getMessage + Console.RESET)
//      }
//      finally println("It was close")
//
//    println(result_of_divining_by_zero)
//
//    println("-" * 40)
//
//    val List(_, _, third, fourth, rest @ _*) = List(1, 2, 3, 4, 5, 6, 7)
//
//    println(third)
//    println(fourth)
//    println(rest)
//
//    println("-" * 40)
//
//    val result_of_list_values =
//      List(1,2,3,4,5,6,7,8,9) match {
//        case List(_, _, third, fourth, _*) if third == fourth -1 => true
//        case _ => false
//      }
//
//    println(result_of_list_values)
//
//    println("-" * 40)
//
//    println(
//      List(6, 2, 5, 9, 8).sortWith((a, b) => a <= b)
//    )
//    println(
//      List(6, 2, 5, 9, 8).sortWith((a, b) => a >= b)
//    )
//    println(
//      List(6, 2, 5, 9, 8).sortWith(_ >= _)
//    )
//
//    println("-" * 40)
//    println("-" * 40)
//
//    def higher_order_function(f: Int => String): Unit = {
//      var acc = 2
//
//      while (acc <10) {
//        println(f(acc))
//
//        acc += 1
//      }
//    }
//
//    def rendered(n: Int): String =
//      Console.YELLOW + n + Console.RESET
//
//    higher_order_function(rendered)
//
//    println("-" * 40)
//
//    higher_order_function {
//      case 4 => Console.CYAN + 4 + Console.RESET
//      case x => x.toString
//    }
//
//    println("-" * 40)
//
//    higher_order_function {
//      case x if x % 3 == 0 => Console.CYAN + x + Console.RESET
//      case x => x.toString
//    }
//
//
//
//    println("-" * 40)
//
//    val x: Unit =
//      List(1, 2, 3).foreach[Unit](println)
//
//    val f1: Int => String = rendered
//    val f2 = rendered _
//    val f3 = x => rendered(x)
//    val f4 =      rendered(_)
//
//    println("-" * 40)
//    higher_order_function(f1)
//    println("-" * 40)
//    higher_order_function(f2)
//    println("-" * 40)
//    higher_order_function(f3)
//    println("-" * 40)
//    higher_order_function(f4)
//
//    println("-" * 40)
//
//    @scala.annotation.tailrec
//    def loop(acc: Int): Unit =
//      if(acc < 10) {
//        println(acc)
//
//        loop(acc + 1)
//      }
//
////    loop(5)
//
//    def interesting(): Unit = {
//      var acc = 3
//
//      while(acc < 10) {
//        println(acc)
//
//        acc += 1
//      }
//    }
//
//    interesting()
//
//    println("-" * 40)
//
//    def spark_or_scala(
//      spark: Int,
//      scala: Int
//    ): String =
//      if (spark * scala == 30)
//        "exacly"
//      else if (spark * scala == 60)
//        "it's 60"
//      else
//        "type again"
//
//    println(spark_or_scala(spark = 5, scala = 7))
//    println(spark_or_scala(spark = 5, scala = 6))
//    println(spark_or_scala(spark = 5, scala = 12))
//    println(spark_or_scala(spark = 5, scala = 3))
//
//    println("-" * 40)
//
    def wanna_watch_movie_and_go_to_ice_cream(
      movie: Boolean,
      ice_cream: Boolean
    ): String =
    if (movie && ice_cream)
      "awesome"
    else if (movie || ice_cream)
      "still cool"
    else
      "nuda"
//
//    println(wanna_watch_movie_and_go_to_ice_cream(movie = false, ice_cream = false))
//    println(wanna_watch_movie_and_go_to_ice_cream(movie = false, ice_cream = true))
//    println(wanna_watch_movie_and_go_to_ice_cream(movie = true, ice_cream = false))
//    println(wanna_watch_movie_and_go_to_ice_cream(movie = true, ice_cream = true))
//
//    println("-" * 40)
//
//    val result = if (true) "good" else throw new Exception
//
//    println(result)
//
//    println("-" * 40)

//    def some_string_operations(nickname: String = "Example"): String = {
//      println("Tester".equals(nickname)) // is string equal
//      val nickname_array = nickname.toArray
//      for (v <- nickname_array) {
//        println(v)
//      }
//    }
  }
}