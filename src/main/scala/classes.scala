//import com.sun.org.apache.xalan.internal.utils.XMLSecurityManager.printWarning

class MovePoint(var x: Int, var y: Int) {

  def move(dx: Int, dy: Int): Unit = {
    x = x + dx
    y = y + dy
  }

  override def toString: String =
    s"($x, $y)"
}

//    CONSTRUCTORS
class ConstructPoint(var x: Int = 0, var y: Int = 0)

//class AnotherConstruct(
//                      var x: Int = 0,
//                      var y: Int = 0
//                      )

//    PRIVATE MEMBERS AND GETTER/SETTER SYNTAX

class PrivatePoint {
  private var _x = 0
  private var _y = 0
  private val bound = 100

  def x = _x
  def x_= (new_value: Int): Unit = {
    if (new_value < bound) _x = new_value else print_warning
  }

  def y = _y
  def y_= (new_value: Int): Unit = {
    if (new_value < bound) {
      _y = new_value
      println(y)
    } else
      print_warning
  }

  private def print_warning = println("WARNING: Out of bounds")
}

//    methods def x and def y are created for accessing the private data.
//    def _x = is for validating and setting the value of _x
//    special syntax for setters: the method has _= appended to the identifier
//    of the getter and the parameters come after.

class SetPoint(val x: Int, val y: Int)


object classes {
  def main(args: Array[String]): Unit = {

    val point1 = new MovePoint(2, 4)
    println(point1)
    println(point1.x)
    point1.move(2, 2)
    println(point1.x)

    println("*" * 40)

    var point2 = new MovePoint(2, 4)
    println(point2)
    println(point2.x)
    point2.move(2, 2)
    println(point2.x)

    println("*" * 40)

    val origin = new ConstructPoint
    val new_origin = new ConstructPoint(2, 5)
    println(new_origin.x)

    val newer_origin = new ConstructPoint(y = 8)
    println(newer_origin.y)

    println("*" * 40)

    val point3 = new PrivatePoint
    point3.x = 312
    point3.y = 12

    println("*" * 40)

    val point4 = new SetPoint(1, 2)
//    try point4.x = 4  Error cause SetPoint arguments are set as values.
  }
}
