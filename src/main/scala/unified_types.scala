object unified_types {
  def main(args: Array[String]): Unit = {
   println("Hello World:")

   println("*" * 40)

  val list: List[Any] = List(
    "a string",
    732,      // an integer
    "c",      // a character
    true,     // boolean value
    () -> "anoymous function which returns string or list(?)"
  )

  val x: Long = 4123412
  val y: Float = x

  val face: Char = '☺'
  val number: Int = face

  println(y)


  list.foreach(element => println(element))

  }
}
